﻿Public Structure LockBoxFile
    Public Filename As String
    Public isFolder As Boolean
    Public isLocked As Boolean
End Structure


Public Class cLockBoxHelper

    Public Folder As String = "D:\LibDemPrivate"

    Function fileList()
        Dim aRetVal As New List(Of LockBoxFile)
        Dim file As LockBoxFile

        Dim files() As String = IO.Directory.GetFiles(Me.Folder)
        Dim folders() As String = IO.Directory.GetDirectories(Me.Folder)

        For Each fileName As String In files
            file.Filename = fileName
            file.isFolder = False
            aRetVal.Add(file)
        Next

        For Each fileName As String In folders
            file.Filename = fileName
            file.isFolder = True
            aRetVal.Add(file)
        Next

        Return aRetVal
    End Function



End Class
